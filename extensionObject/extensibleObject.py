class ExtensibleObject():
    """
    Generic class for creating an extensible object
    """

    def __init__(self):
        # Extensions of this object. Extensions add functionality to an extensible object.
        self.extensions = []

    def select(self, extension):
        """
        Select an extensible object based on its extensions. It needs to have all of the listed extensions, so that this function returns true.
        """
        try:
            for e in extension:
                if self.getExtension(e) is None:
                    return False
        except TypeError:  # extension not an iterable?
            if self.getExtension(extension) is None:
                return False

        return True

    def listExtensions(self):
        """
        List all extensions of an extensible object as a string
        """
        separator = ','
        return separator.join([ext.name for ext in self.extensions])

    def deleteExtension(self, ext):
        """
        Delete a particular extension of an extensible object
        """
        self.extensions.remove(ext)
        del ext

    def clearExtensions(self):
        """
        Clears all exstensions from an extensible object
        """
        for ext in self.extensions:
            self.deleteExtension(ext)

    def getExtension(self, extClass):
        """
        Returns the extension object for a given extension class from an extensible object
        """
        for extension in self.extensions:
            if isinstance(extension, extClass):
                return extension

        return None

    def hasExtension(self, extClass):
        """
        Checks for existence of an extension in an extensible object
        """
        if self.getExtension(extClass) is None:
            return False
        else:
            return True

    def addExtension(self, ext, *args, **kwargs):
        """
        Adds an extension to an extensible object. Only one extension of each kind is allowed!
        """
        if not self.hasExtension(type(ext)):
            self.extensions.append(ext(self, *args, **kwargs))
            return True

        else:
            return False