import concurrent.futures as cf
import logging

class BatchExecutor():
    """
    Executes tasks for extensible objects that have a set of extensions

    A function can be provided that is always called prior to execution
    """

    def __init__(self, objects, logger = None, parallel_threads = 10):
        # The list of objects on which to execute operations
        self.objects = objects

        # Large thread pool in which parallel threads are started
        self.threadPoolExecutor = cf.ThreadPoolExecutor(parallel_threads)

        if logger is not None:
            self.logger = logging.getLogger(logger)
        else:
            self.logger = logging.getLogger("general_logs.Executor")

    def execute(self, ext, fn, *args, selector=None, **kwargs):
        """
        Executes any function on an extension 
        and jumps to the callback or multiple callbacks after all tasks are finished

        The object needs to have the extension 'ext' for a successful execution.
        Optionally, the 'selector' is a list of extensions that an object is required to have, in order for an execution to occur.
        """
        # List of tasks and active objects
        futures = []
        active = []
        matches = 0

        for o in self.objects:
            if selector is not None:
                if self.select(o, selector):
                    # The object was selected, as it has all the required extensions
                    pass
                else:
                    # Skip this loop iteration, because the object was not selected
                    continue

            # Object selected. Proceed with execution
            extension = o.getExtension(ext)

            # Does the object have the required extension?
            if extension is not None:
                matches += 1
                future = self.threadPoolExecutor.submit(
                    fn, extension, *args, **kwargs)

                futures.append(future)
                active.append(o)

        if matches > 0:
            self.logger.info(
                "Running task on a total of %d instances." % matches)
        else:
            self.logger.info(
                "There are no instances to run this task on.")

        cf.wait(futures)

        return futures